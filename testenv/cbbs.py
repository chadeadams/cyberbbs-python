#!bin/python
#================================================================================
# CyberBBS Bulletin Board System
# Written in Python3+
# Author Chad Adams/Nugax
# cadams@cyberbbs.com
#
#================================================================================

import sys, cbbsio, servers


#Start Telnet Server
servers.start_telnet_server()
