#!bin/python
#================================================================================
# CyberBBS Bulletin Board System
# Written in Python3+
# Author Chad Adams/Nugax
# cadams@cyberbbs.com
#
#=============================================================================

#Input/Output Routines For CyberBBS

import sys, servers, bbs

#Setup Reader And Writer Objects
reader = 0
writer = 0

#Telnet Socket Write
def writeline(str):
    str = str + "\r\n"
    writer.write(str)

#Telnet Socket Read Full Line (CR Completed)
def readline():
    sInput = yield from reader.readline()
    return sInput

#Read One Character
def readchar():
    cInput = yield from reader.read(1)
    return cInput

#Yes/No Prompt Read
