##!bin/python
#================================================================================
# CyberBBS Bulletin Board System
# Written in Python3+
# Author Chad Adams/Nugax
# cadams@cyberbbs.com
#
#================================================================================

import sys, cbbsio, time, cbbsinfo

#Start BBS Code

#Display Connection Banner
def display_banner():
    sBBSName = cbbsinfo.cbbs_info['cyberbbs_header']
    sVersion = cbbsinfo.cbbs_info['cyberbbs_version']
    sOS      = cbbsinfo.cbbs_info['os']
    sProc    = cbbsinfo.cbbs_info['cpu_long']
    sConnectBanner = 'Connected: ' + sBBSName + ' For ' + sOS + ' ' + sProc
    sAuthor = "By: " + cbbsinfo.cbbs_info['cyberbbs_author']
    #Write Connect Banner
    cbbsio.writeline(sConnectBanner)
    #Write Author
    cbbsio.writeline(sAuthor)
    #Sleep 3 Seconds
    time.sleep(3)

#Start The BBS
def start_bbs():
    display_banner();
