#bin/python
#================================================================================
# CyberBBS Bulletin Board System
# Written in Python3+
# Author Chad Adams/Nugax
# cadams@cyberbbs.com
#
#================================================================================

import sys, asyncio, telnetlib3

#Server File




#Telnet server
#Start Telnet Server
def start_telnet_server:
    port = 6023
    loop = asyncio.get_event_loop()
    coro = telnetlib3.create_server(port=port, shell=start.start_cbbs)
    #Log OutPut to File and Console

    print "\r\n\r\n"
    print("CyberBBS Telnet Server Started...\r\n")
    print("Running on Port: " + port + "\r\n")
    print ("Waiting for connection... : ")

    server = loop.run_until_complete(coro)
    loop.run_until_complete(server.wait_closed())
