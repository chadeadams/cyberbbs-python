#!bin/python
#================================================================================
# CyberBBS Bulletin Board System
# Written in Python3+
# Author Chad Adams/Nugax
# cadams@cyberbbs.com
#
#=============================================================================

#CyberBBS BBS Info

import sys, platform

#======================================================
#Global BBS Variables
#======================================================
cbbs_info = {
    'cyberbbs_version' : '1.0.3',
    'cyberbbs_header'  : 'CyberBBS Bulletin Board System',
    'cyberbbs_author'  : 'Chad Adams (Nugax)',
    'cpu_long'         : platform.processor(),
    'python_ver'       : platform.python_version(),
    'os'               : platform.system(),

}
